"use strict"
require("./model/mongooseConnection")
const express = require("express");

const bodyparser = require("body-parser")

const port =8080

const app = express();

const ejs = require("ejs");

const crypto = require("crypto")

const multer = require("multer")

const GridFsStorage = require("multer-gridfs-storage")

const mongoose= require("mongoose")

const methodoverride = require("method-override")

const path = require('path')

const fs = require('fs');

let uri=require("./model/mongooseConnection")

const Handlebars=require("handlebars");

const photos_directory="/home/ardrlabs/Dinesh/fe-be/resumemaker/src/image/image.jpeg"

var pdfMake = require('pdfmake/build/pdfmake.js');

var pdfFonts = require('pdfmake/build/vfs_fonts.js');

var Module = require('module');

pdfMake.vfs = pdfFonts.pdfMake.vfs;

//const template = Handlebars.compile("Name: {{name}}");

//const fun=require("./utils/templategenerater")

//fun.createtemplate()

//middleware

app.use(bodyparser.urlencoded({extended:true}))

app.use(bodyparser.json({extended:true}))

app.use(methodoverride('_method'))

app.use(express.static("src"))

app.set("view engine",'ejs')


const storage = multer.diskStorage({
    destination:"./src/uploads",
    filename: function(req,file,cb){
      cb(null,file.feildname+'-'+Date.now()+path.extname(file.originalname))
    }
})

// var toDataUrl=(flag,function(base64Image){
//     flag_pdf.push(base64Image);
//  });
// var image = _.range(1,18,1);
// _.each(image,function(obj){
//      var goal_image = 'src/images/escap/'+obj+'.png';
//      toDataUrl(goal_image,function(base64Image){
//         flag_pdf.push(base64Image);
//      });
// });


/*
by using fgridfs storage we can store it in mongodb
*/
// const storage = new GridFsStorage({
//     url:uri,
//     file: (req, file) => {
//       return new Promise((resolve, reject) => {
//         crypto.randomBytes(16, (err, buf) => {
//           if (err) {
//             return reject(err);
//           }
//           const filename = buf.toString('hex') + path.extname(file.originalname);
//           const fileInfo = {
//             filename: filename,
//             bucketName: 'userimage'
//           };
//           resolve(fileInfo);
//         });
//       });
//     }
//   });

 let upload = multer({ storage :storage }).single("image");

 let resumemodel = mongoose.model("resume")

//@routes

//get homepage
//@discreption to get home page
// app.get("/",(req,res)=>{

//    // res.render("index")
// })

app.get("/",(req,res)=>{

    res.render("index")
})



app.post("/upload",upload,(req,res)=>{

    // fs.unlink('./src/uploads', (err) => {
    //     if (err) throw err;
    //     console.log('successfully deleted /tmp/hello');
    //   });
          
            resumemodel.deleteMany((error,data)=>{
                if(error){
                    res                   
                    .json({message:error})
                }else{                    
                    if(req.body.firstname&&req.body.lastname&&req.body.toolbox&&req.body.currentpostion&&req.body.Profile&&req.body.Workexperience&&req.body.fromdate&&req.body.todate&&req.body.designation&&req.body.city&&req.body.country&&req.body.Projectname&&req.body.discription&&req.body.responcibilities&&req.body.technologies&&req.body.Education&&req.file.filename){

                        let resumedata= new resumemodel({ 

                            Firstname:req.body.firstname,
              
                            Lastname:req.body.lastname,
              
                            toolbox:req.body.toolbox,
              
                            Currentposition:req.body.currentpostion,
              
                            Profile:req.body.Profile,
                      
                            Workexperience:req.body.Workexperience,
                      
                            Fromdate:req.body.fromdate,
                          
                            Todate:req.body.todate,
                          
                            Designationname:req.body.designation,

                            city:req.body.city,
              
                            Projectname:req.body.Projectname,
              
                            discription:req.body.discription,
              
                            responsibilities:req.body.responcibilities,
              
                            technologies:req.body.technologies,
                            
                            Workexperience2:req.body.Workexperience2,
                      
                            Fromdate2:req.body.fromdate2,
                          
                            Todate2:req.body.todate2,
                          
                            Designationname2:req.body.designation2,

                            city2:req.body.city2,
              
                            Projectname2:req.body.Projectname2,
              
                            discription2:req.body.discription2,
              
                            responsibilities2:req.body.responcibilities2,
              
                            technologies2:req.body.technologies2,

                            Workexperience3:req.body.Workexperience3,
                      
                            Fromdate3:req.body.fromdate3,
                          
                            Todate3:req.body.todate3,
                          
                            Designationname3:req.body.designation3,

                            city3:req.body.city3,
              
                            Projectname3:req.body.Projectname3,
              
                            discription3:req.body.discription3,
              
                            responsibilities3:req.body.responcibilities3,
              
                            technologies3:req.body.technologies3,

                            Education:req.body.Education,
                      
                            certifications:req.body.certifications,
                      
                            others:req.body.others,
                          
                            photourl:req.file.filename
                      
                          })
                              resumedata.save((error,doc)=>{
                          if(error){
                              console.log(error);
                               
                          }else{
                               res        
                               .render("ack",{
                                   doc:doc
                               })
                               
                          }
                       });       
                      
                      }else{
                          res        
                         .render("index",{
                          message:"required fields are missing"                  
                          })
              
                      }
                }
            })   

    }) 

    
    
    app.get("/download",(req,res)=>{
        
        return resumemodel.findOne()
        .then(record=>{ 
            console.log(record);
          
var dd = {
                     
    pageSize: 'A4',
    pageMargins: [ 40, 40, 40, 40 ],                 
            
    content: [
        {
            text:[ `${record.Firstname}`,  {text:`${record.Lastname}`}],
           alignment: 'left',
           style: 'ufirstname'
        },
        
            {
            text:`${record.Currentposition}`,
            alignment: 'left',
            style: 'currentposition'
        }, 
           {
              text:"___________________________________________________________________",
              style: 'subheading'
            },
           {
              text:"PROFILE:",
              style: 'subheading'
            },
            {
             text:`${record.Profile}`,
             style: 'body'
                
            },
            {
              text:"___________________________________________________________________",
              style: 'subheading'
            },
            {
              text:"TOOLBOX:",
              style: 'subheading'
            },
             {
             text:`${record.toolbox}`,
             style: 'body'
                
            },
            {
                text:"___________________________________________________________________",
                style: 'subheading'
            },
            
                
       
      ],
      
      styles:{
         ufirstname:{
             fontSize: 20,
           bold: true
         },
         subheading:{
             fontSize: 17,
          
         },
         currentposition:{
             fontSize: 15,
         },
         body:{
             fontSize: 12,
             alignment: 'justify'
         }
      }

}                 

                
            
            const pdfdcc = pdfMake.createPdf(dd)  ;
            pdfdcc.getBase64((data) => {
                res.writeHead(200,{
                    "content-type":"Application/pdf",
                    "content-disposition":'attachment;filename="resume.pdf'
                })
             const download = Buffer.from(data.toString('utf-8'),'base64')                        
             res.end(download)            
            });        
        }).catch(error=>{
            console.log(error);
            
        })
    })


app.listen(port,()=>{
    console.log("server running on port",port);    
});






