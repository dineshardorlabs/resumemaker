var mongoose = require('mongoose')
var resumeSchema =  new mongoose.Schema({
    Firstname:{
        type:String,        
    },
    Lastname:{
        type:String,        
    },
    toolbox:{
        type:String,        
    },

    Currentposition:{
        type:String,        
    },

    Profile:{
        type:String,        
    },

    Workexperience:{
        type:String,        
    },

    Fromdate:{
        type:String,       
    },

    Todate:{
        type:String,       
    },

    Designationname:{
        type:String,       
    },

    city:{
        type:String,       
    },

    Projectname:{
        type:String,       
    },

    discription:{
        type:String,       
    },

    responsibilities:{
        type:String,       
    },

    technologies:{
        type:String,       
    },
    Workexperience2:{
        type:String,        
    },

    Fromdate2:{
        type:String,       
    },

    Todate2:{
        type:String,       
    },

    Designationname2:{
        type:String,       
    },

    city2:{
        type:String,       
    },

    Projectname2:{
        type:String,       
    },

    discription2:{
        type:String,       
    },

    responsibilities2:{
        type:String,       
    },

    technologies2:{
        type:String,       
    },
    
    Workexperience3:{
        type:String,        
    },

    Fromdate3:{
        type:String,       
    },

    Todate3:{
        type:String,       
    },

    Designationname3:{
        type:String,       
    },

    city3:{
        type:String,       
    },

    Projectname3:{
        type:String,       
    },

    discription3:{
        type:String,       
    },

    responsibilities3:{
        type:String,       
    },

    technologies3:{
        type:String,       
    },

    Education:{
        type:String,       
    },

    certifications:{
        type:String,       
    },

    others:{
        type:String,       
    },

    photourl:{
        type:String,        
    }
   
})

mongoose.model("resume",resumeSchema,)